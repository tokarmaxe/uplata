<?php

namespace app\Additions\Traits;


trait SetOpts
{
    public function setOpsDefault($url,$data=null, $cookiefile='tmp/cookie.txt')
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');

        curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiefile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiefile);

        if($data){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        $html = curl_exec($ch);

        curl_close($ch);

        return $html;
    }
}