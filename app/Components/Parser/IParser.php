<?php

namespace app\Components\Parser;


interface IParser
{
    public function parseUrl($url);
}