<?php

namespace app\Components\Parser;

use app\Additions\Traits\SetOpts;

class Parser implements IParser
{
    use SetOpts;

    public function parseUrl($url)
    {
        $html = iconv('windows-1251', 'utf-8', $this->setOpsDefault($url));

        $q = new \simple_html_dom();
        $q->load($html);
        foreach ($q->find('div#postlist ol li') as $l){
            $g = str_get_html($l);
            $title = $g->find('h2.title', 0)->plaintext;
            echo '<br>';
            $author = $g->find('a.username', 0)->plaintext;
            echo '<br>';
            $data = str_replace("&nbsp;", " ", $g->find('span.date', 0)->plaintext);
            echo '<br>';
            $content = $g->find('blockquote.postcontent', 0)->plaintext;
            $str = $title.$author.$data.$content;
            var_dump(fopen("data".DIRECTORY_SEPARATOR.$title.$data.'.txt','w'));
            file_put_contents("data".DIRECTORY_SEPARATOR.$title.$data.'.txt', $str);
        }
    }
}