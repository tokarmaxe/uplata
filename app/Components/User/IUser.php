<?php

namespace app\Components\User;


interface IUser
{
    public function login($url, $data, $cookiefile);
}