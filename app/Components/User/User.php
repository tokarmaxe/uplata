<?php

namespace app\Components\User;

use app\Additions\Traits\SetOpts;

class User implements IUser
{
    use SetOpts;

    function login($url, $data, $cookiefile)
    {
        $this->setOpsDefault($url, $data, $cookiefile);
    }
}