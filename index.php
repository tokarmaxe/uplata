<?php

require_once 'vendor/autoload.php';
require_once 'phpdom/simple_html_dom.php';

use app\Components\User\IUser;
use app\Components\User\User;
use app\Components\Parser\IParser;
use app\Components\Parser\Parser;
use app\Additions\Traits\SetOpts;

class Program
{
    use SetOpts;

    private $user, $parser, $config;

    public function __construct(IUser $user, IParser $parser)
    {
        $this->user = $user;
        $this->parser = $parser;
        $this->config = require_once('configs/config.php');
    }

    public function exec()
    {
        $this->user->login($this->config['urlLoginForm'], $this->config['userInfo'], $this->config['cookiePath']);
        $this->parser->parseUrl($this->config['urlToParse']);
    }
}

$program = new Program(new User, new Parser);
$program->exec();